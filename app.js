// app.js

// BASE SETUP
// =============================================================================

// call the packages we need
GLOBAL.express    = require('express'); 		// call express
GLOBAL.app        = express(); 				// define our app using express
GLOBAL.bodyParser = require('body-parser');
GLOBAL.Moniker = require('moniker');
GLOBAL.validator = require('validator');
GLOBAL._ = require('lodash');
//log4js setup
GLOBAL.log4js = require('log4js');
log4js.configure({
  appenders: [
    { type: 'console' },
    { type: 'file', filename: 'logs/Server.log', category: 'Server' }
  ]
});
GLOBAL.logger = log4js.getLogger('Server');
GLOBAL.morgan     = require('morgan');
GLOBAL.anagrams = {
};
var routes     = require('./routes/routes');
GLOBAL.anagram     = require('./models/anagram');
routes.logger = logger;
app.use(bodyParser.json({parameterLimit: 10000,limit: '50mb'}));
app.use(bodyParser.urlencoded({parameterLimit: 10000,limit: '50mb', extended: true}));
var port = process.env.PORT || 8083; 		// set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); 				// get an instance of the express Router
app.set('view engine', 'ejs');
app.use(morgan('dev'));
GLOBAL.PC = require("permcomb");
// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
        logger.debug(req.body);
	logger.info('IP = '+req.ip);
	next(); // make sure we go to the next routes and don't stop here
});

router.get('/', function(req, res) {
	res.render('index');
});


router.route('/permutate/:str')
        .get(routes.permutate);
router.route('/dictionary/:str')
        .get(routes.dictionary);

// all of our routes will be prefixed with /api
app.use('/api', router);
anagram.usingItNow(anagram.myCallback);
// START THE SERVER
// =============================================================================
app.listen(port);
logger.info('Server is running on port ' + port);
