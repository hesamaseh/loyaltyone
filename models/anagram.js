var _this = this;
exports.findAnagrams = function ( sortedWord ) {

        if( anagrams[sortedWord] ) {
                return anagrams[sortedWord];
        }

        return [];
};

exports.addAnagram =  function ( sortedWord, word ) {

        if( anagrams[sortedWord] ) {
                anagrams[sortedWord].push(word);
        } else {
                anagrams[sortedWord] = [ word ];
        }

};
exports.myCallback = function (data) {
  var names = Moniker.generator([Moniker.adjective, Moniker.noun, Moniker.verb]);
  logger.info(names.dicts.length);
  for(var i = 0; i < names.dicts.length; i++)
  {
        //logger.debug(names.dicts.words.length);
        logger.debug(names.dicts[i].words.length);
        for(var j = 0; j < names.dicts[i].words.length; j++)
        {
                //logger.debug(names.dicts[i].words[j]);
                var arr = [];
                for (var k = 0; k < names.dicts[i].words[j].length; k++) {
                    arr.push(names.dicts[i].words[j][k]);

                }
                //logger.debug(arr.sort());
                //logger.debug(arr.sort().join(''));
                _this.addAnagram(arr.sort().join(''),names.dicts[i].words[j]);
        }
  }
};

exports.usingItNow = function(callback) {
  callback('get it?');
};
